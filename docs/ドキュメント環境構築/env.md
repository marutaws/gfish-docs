# ドキュメント環境構築手順

## 概要

このドキュメントを作成するに当たり、以下のツールを使用しています。

|  No   |      ツール名       |                 用途                 |                                           URL                                            |
| :---: | :------------------ | :----------------------------------- | :--------------------------------------------------------------------------------------- |
|   1   | Markdown            | マークアップ言語                     | [DARING FIREBALL](https://daringfireball.net/projects/markdown/)                         |
|   2   | PlantUML            | UML生成言語                          | [PlantUML](https://plantuml.com/ja/)                                                     |
|   3   | MkDocs              | 静的サイトジェネレーター             | [MkDocs](https://www.mkdocs.org/)                                                        |
|   4   | BitBucket           | Gitリポジトリ管理 & 一時ホスティング | [BitBucket](https://bitbucket.org)                                                       |
|   5   | BitBucket Pipelines | CI/CDツール                          | [BitBucket Pipeline](https://www.atlassian.com/ja/software/bitbucket/features/pipelines) |

以下の流れで使用しています(未稼働部分含む)。

```plantuml
partition 編集者のローカルでの作業 {
    :ドキュメント用リポジトリをBitBucketからクローン;
    :Markdown記法とPlantUML記法を使用して仕様書を作成・編集する。;
    :mkDocsで静的サイトに変換し、内容を確認する。;
    :レビュー後変更をコミットする;
    :BitBucketに変更をプッシュする;
}
partition BitBucket側での自動作業 {
    :pipelinesからMkDocsを使用して静的サイトに変換;
    if (pipelineにエラー?) then (yes)
        :変換エラーがあったことをメールで通知;
        end
    else (no)
    endif
    :生成された静的サイトをBitBucketのドキュメント用リポジトリにプッシュ;
}
:公開完了;

```

## インストール方法

### MkDocs

1. Pythonのインストール
    MkDocsはPythonで作られており、pipで公開されています。  
    そのため、まずはPythonをインストールします。  
    [このあたり](https://www.python.jp/install/macos/install_python.html)を参考にしてください。

1. MkDocsのインストール
   pipを使用してインストールします。

    ```console
    pip install mkdocs
    ```

    MkDocsで必要な拡張機能もインストールします。

    ```console
    pip install pymdown-extensions
    pip install plantuml-markdown
    pip install mkdocs-material
    ```

1. ドキュメントプロジェクトのクローン

    以下を実行してプロジェクトをクローンします。

    ```console
    git clone https://yosuke_maruta@bitbucket.org/marutaws/gfish-docs.git
    ```

1. ファイルを編集します。

### PlantUML

1. Graphvizのインストール

    以下を実行して図形描写ライブラリのGraphvizをインストールします。

    ```console
    brew install graphviz
    ```

1. PlantUMLのインストール

    以下を実行してPlantUMLをインストールします。

    ```console
    brew install plantuml
    ```

以上でインストールは完了です。

## 編集ツールの設定

Markdownは使い慣れたエディタで編集してもらえばOKです。  
要件としては以下があればいいかと思います。

- Markdownの中にPlantUMLを入れ込んで表示ができる。

サンプルとしてVisualStudioCodeとIntelliJの設定を書いておきます。

### VisualStudioCodeの設定

1. VisualStudioCodeのインストール

    以下を実行してVisualStudioCodeをインストールします。

    ```console
    brew install visual-studio-code --cask
    ```

1. プラグインのインストール

    以下のプラグインをインストールしてください。

    |  No   |       プラグイン名        |                  用途                  |                                                         URL                                                          |
    | :---: | :------------------------ | :------------------------------------- | :------------------------------------------------------------------------------------------------------------------- |
    |   1   | Markdown All in One       | マークダウン編集補助                   | [Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one)                |
    |   2   | Markdown Preview Enhanced | マークダウンプレビュー                 | [Markdown Preview Enhanced](https://marketplace.visualstudio.com/items?itemName=shd101wyy.markdown-preview-enhanced) |
    |   3   | markdownlint              | マークダウンLinter                     | [markdownlint](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint)                   |
    |   4   | Table Formatter           | マークダウンのテーブルフォーマット調整 | [Table Formatter](https://marketplace.visualstudio.com/items?itemName=shuworks.vscode-table-formatter)               |

    その他便利そうなプラグインを試してみてください。

### IntelliJの設定

IntelliJはインストール済みの前提です。

1. 以下のプラグインをインストールしてください。

    |  No   |     プラグイン名     |       用途       |                                          URL                                           |
    | :---: | :------------------- | :--------------- | :------------------------------------------------------------------------------------- |
    |   1   | PlantUML integration | PlantUML編集機能 | [PlantUML integration](https://plugins.jetbrains.com/plugin/7017-plantuml-integration) |

    インストール後にIntelliJを再起動してください。

1. Preference -> Languages & Frameworks -> Markdown を開き、Markdown ExtensionsのリストからPlantUMLにチェックを入れてください。

    ![Intellij](../images/env/env_intellij.png)

    こちらはLinterが見当たらなかったので、できるだけきれいに書いてください！(笑)

## プレビュー方法

MkDocsのプレビュー方法を説明します。

1. 簡易サーバーを起動する場合

    mkdocs.ymlがあるフォルダで以下のコマンドを実行することで、簡易サーバーを起動できます。

    ```console
    mkdocs serve
    ```

    起動後、[http://127.0.0.1:8000] にアクセスして内容を確認してください。

1. htmlファイルを生成する場合

    mkdocs.ymlがあるフォルダで以下のコマンドを実行することで、htmlファイルを生成できます。

    ```console
    mkdocs build
    ```

    実行後、「site」というディレクトリが生成されているので、site/index.htmlをブラウザで開いて内容を確認してください。
