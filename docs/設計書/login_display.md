# ログイン画面設計書

## 画面概要

- ログインを実施する

## 画面

![display](../images/login/login_display.png)

|  No   |    名称    |   タイプ   | 最大桁数 | アクション |
| :---: | :--------- | :--------- | :------: | :--------- |
|  (1)  | ユーザーID | テキスト   |   100    | -          |
|  (2)  | パスワード | パスワード |   100    | -          |
|  (3)  | ログイン   | ボタン     |    -     | 1          |
|  (4)  | メッセージ | テキスト   |    -     | -          |

## アクション

1. ログイン実施
    1. フローチャート

        ```plantuml
        start
        :Input
        ・ユーザーID
        ・パスワード;
        :ログインAPIを実行;
        if (エラーレスポンス受信?) then (yes)
            :画面にエラーメッセージを表示;
            end
        endif

        :セッションにログイン情報をセット;
        :メイン画面に遷移;
        end
        ```

    1. 処理詳細
        1. フォームより以下のInputデータを取得する
           - ユーザーID
           - パスワード

        1. 以下の内容でログインAPIを実行

            ```json
            "request" {
                "userId" : "{Input.ユーザーID}",
                "password" : "{Input.パスワード}"
            }
            ```

        1. ログインAPIからエラーレスポンスが戻ってきた場合、メッセージフォームにエラーメッセージを表示する。

        1. ログインに成功した場合、セッションにログイン情報をセットする。

        1. メイン画面に遷移する。
