# ログインAPI設計書

## API概要

- ユーザー名のバリデーションを実施する
- パスワードのバリデーションを実施する
- ログイン処理を実施する

## フローチャート

``` plantuml
start
:Input
・ユーザー名(userId)
・パスワード(password);
partition バリデーション {
    if (ユーザー名未入力？) then
        :エラーレスポンスを返送;
        end
    endif

    if (パスワード未入力?) is (yes) then 
        :エラーレスポンスを返送;
        end
    endif
}

partition ログイン処理 {
    :ログイン処理実施;
    if (ログイン成功?) then (no)
        :エラレスポンスを返送;
        end
    endif
}
:ログイン処理レスポンスを返送;

end
```

## 処理詳細

1. インプット

|  No   |  データ名  |  変数名  | データ型 |
| :---: | :--------- | :------- | :------- |
|   1   | ユーザー名 | userId   | String   |
|   2   | パスワード | password | String   |

1. バリデーションを実施
    1. ユーザー名が未入力か確認する。未入力の場合は以下のレスポンスを返送する

        ```json
        response {
            "message_id": "user_id_not_entered",
            "messages": {
                "JP": "ユーザー名を入力してください。 ",
                "EN": "Please enter your user id."
            }
        }
        ```

    1. パスワードが未入力か確認する。未入力の場合は以下の以下のレスポンスを返送する

        ```json
        response {
            "message_id": "password_not_entered",
            "messages": {
                "JP": "",
                "EN": "Please enter your password."
            }
        }
        ```

1. ログイン処理を実施
    1. 以下の条件でユーザー情報(userInfo)を取得する。

        ```sql
        SELECT 
            user.user, 
            user.password,
            role.role_id
        FROM
            user
        INNER JOIN 
            role
        ON 
            user.user_id = role.user_id
        WHERE
            user.user_id = :input.userId,
            user.password = :input.password

        ```

    1. userInfoに有効なユーザーが無い場合は以下のメッセージを表示する

        ```json
        response {
            "message_id": "login_failed",
            "messages": {
                "JP": "ログインに失敗しました。ユーザー名とパスワードを確認してください。",
                "EN": "Login failed. Please check your userid and password."
            }
        }
        ```

1. 以下のレスポンスを返送する

    ```json
    response {
        "login_result": true
    }
    ```
